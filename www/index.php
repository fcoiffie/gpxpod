<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>GpxPod
</title>
        <link rel="shortcut icon" href="images/gpxpod.ico" />
        <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
        <link rel="stylesheet" href="css/L.Control.MousePosition.css" />
        <link rel="stylesheet" href="css/leaflet.label.css" />
        <link rel="stylesheet" href="css/Control.Geocoder.css" />
        <link rel="stylesheet" href="css/leaflet-sidebar.css" />
        <link rel="stylesheet" href="css/Control.MiniMap.css" />
        <link rel="stylesheet" href="css/jquery-ui.min.css" />
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="js/tablesorter/themes/blue/style.css" />
        <link rel="stylesheet" href="css/Leaflet.Elevation-0.0.2.css" />
        <link rel="stylesheet" type="text/css" href="css/gpxpod.css" />

        <link rel="stylesheet" href="css/MarkerCluster.css" />
        <link rel="stylesheet" href="css/MarkerCluster.Default.css" />
        <link rel="stylesheet" href="css/L.Control.Locate.min.css" />
    </head>
    <body>
<?php
require('conf.php');

$subfolder = "";

// posted, POST is prioritary over GET
if (!empty($_POST)){

    $subfolder = $_POST['subfolder'];
    $path_to_process = $data_folder.'/'.$subfolder;
    if (file_exists($path_to_process) and is_dir($path_to_process)){
        // then we process the folder if it was asked
        if (isset($_POST['computecheck']) and $_POST['computecheck'] == "yes"){
            exec("$path_to_gpxpod $path_to_process", $output, $returnvar);
        }
    }
    else{
        die("$path_to_process does not exist");
    }

}
else{
    if (!empty($_GET)){
        $subfolder = $_GET['subfolder'];
        $path_to_process = $data_folder.'/'.$subfolder;
    }
}

?>

 <div id="sidebar" class="sidebar">
<!-- Nav tabs -->
<ul class="sidebar-tabs" role="tablist">
<li class="active"><a href="#home" role="tab"><i class="fa fa-bars"></i></a></li>
<li><a href="#settings" role="tab"><i class="fa fa-gear"></i></a></li>
<li><a href="#help" role="tab"><i class="fa fa-question"></i></a></li>
</ul>
<!-- Tab panes -->
<div class="sidebar-content active">
<div class="sidebar-pane active" id="home">
            <div id="logo">
                <p align="center"><img src="images/gpxpod.png"/></p>
            </div>
            <hr />
            <div id="folderselection">
                <form name="choosedir" method="post" action="?">
                    <div id="folderrightdiv">
                    <select name="subfolder" id="subfolderselect">
<?php
$dirs = array_filter(glob("$data_folder/*"), 'is_dir');
foreach($dirs as $dir){
    $selected = "";
    // TODO verif si variable existe
    if (basename($dir) === $subfolder){
        $selected = "selected='selected'";
    }
    echo "<option $selected>".basename($dir)."</option>\n";
}
?>
                    </select>
                    <br/>
                    <button id="saveForm" class="uibutton">Display</button>
                    </div>
                    <div id="folderleftdiv">
                        <b id="titlechoosedirform">Folder selection</b>
                        <br/>
                        <div id="computecheckdiv">
                        <input id='computecheck' name='computecheck' type='checkbox' value="yes"><label for='computecheck' id="computechecklabel">Process markers and tracks</label>
                        </div>
                    </div>
                </form>

            </div>
            <div style="clear:both"></div>
            <hr/>
            <div id="options">
                <h3>Options</h3>
                <div id="optionbuttonsdiv">
                    <button id='removeelevation' class="uibutton">Hide elevation profile</button>
                    <br/>
                    <button id='comparebutton'  class="uibutton">Compare selected tracks</button>
                </div>
                <div id="optioncheckdiv">
                    <input id='displayclusters' type='checkbox' checked='checked'><label for='displayclusters'>Display markers</label>
                    <br/>
                    <input id='transparentcheck' type='checkbox' title="Enable transparency when hover on table rows to display track overviews"><label title="Enable transparency when hover on table rows to display track overviews" for='transparentcheck'>Transparency</label>
                </div>
            </div>
            <div style="clear:both"></div>
            <hr/>
            <h3>Tracks inside current view</h3>
            <div id="gpxlist"></div>
<?php

if ($subfolder != ""){
    echo "<p id='markers' style='display:none'>".file_get_contents("$path_to_process/markers.txt")."</p>\n";
    echo "<p id='subfolder' style='display:none'>$subfolder</p>\n";
    echo "<p id='rooturl' style='display:none'>$root_url</p>\n";
    echo "<p id='gpxcomprooturl' style='display:none'>$gpxcomp_root_url</p>\n";
}
?>
</div>
<div class="sidebar-pane" id="settings">
<br/>
<div id="filtertabtitle">
    <b>Filters</b>
    <button id="clearfilter" class="uibutton">Clear</button>
    <button id="applyfilter" class="uibutton">Apply</button>
</div>
<br/>
<br/>
<br/>
<ul id="filterlist">
    <li>
        <b>Date</b><br/>
        min : <input type="text" id="datemin"><br/>
        max : <input type="text" id="datemax">
    </li>
    <li>
        <b>Distance (m)</b><br/>
        min : <input id="distmin"><br/>
        max : <input id="distmax">
    </li>
    <li>
        <b>Cumulative elevation gain (m)</b><br/>
        min : <input id="cegmin"><br/>
        max : <input id="cegmax">
    </li>
</ul>
</div>
<div class="sidebar-pane" id="help"><h1>Help</h1>Shortcuts :
    <ul>
        <li>&lt; : toggle sidebar</li>
        <li>! : toggle minimap</li>
        <li>œ or ² : toggle search</li>
    </ul>
    <br/>
    Features :
    <ul>
        <li>Select folder on top of main sidebar tab and press "Display" load a folder content.</li>
        <li>Click on marker cluster to zoom in.</li>
        <li>Click on track line or track marker to show popup with track stats and a link to draw track elevation profile.</li>
        <li>In main sidebar tab, the table lists all track that fits into current map bounds. This table is kept up to date.</li>
        <li>Sidebar table columns are sortable.</li>
        <li>In sidebar table, [p] link near the track name is a permalink.</li>
        <li>In sidebar table and track popup, click on track links to download the GPX file.</li>
        <li>"Transparency" option : enable sidebar transparency when hover on table rows to display track overviews.</li>
        <li>"Display markers" option : hide all map markers. Sidebar table still lists available tracks in current map bounds.</li>
        <li>Many leaflet plugins are active :
            <ul>
                <li>Markercluster</li>
                <li>Elevation</li>
                <li>Sidebar-v2</li>
                <li>Control Geocoder (search in nominatim DB)</li>
                <li>Minimap (bottom-left corner of map)</li>
                <li>MousePosition</li>
            </ul>
        </li>
    </ul>
</div>
</div>
</div>
</div>
<!-- ============================ -->

        <div id="map" class="sidebar-map"></div>

        <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
        <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
        <script src="js/leaflet.label.js"></script>
        <script src="js/L.Control.MousePosition.js"></script>
        <script src="js/Control.Geocoder.js"></script>
        <script src="js/Control.MiniMap.js"></script>
        <script src="js/L.Control.Locate.min.js"></script>
        <script src="js/leaflet-sidebar.min.js"></script>
        <!--script type="text/javascript" src="http://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script-->
        <script src="js/leaflet.markercluster-src.js"></script>
        <script src="js/Leaflet.Elevation-0.0.2.min.js"></script>
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.mousewheel.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.min.js"></script>
        <script src="js/gpxpod.js"></script>
    </body>
</html>
