<?php
// path that will be scanned to find folders containing gpx files
// !! All folders in this path must be writable by the user running the 
// web server to write markers and geojson files !!
$data_folder = "/home/user/data/gpxs";

// path to python processing script which will be launched by index.php, each time we display a folder
// to produce the geojson files and the markers
$path_to_gpxpod = "/home/user/bin/gpxpod.py";

// Root URL to gpxpod, used to pass GPX urls to an instance of gpx-divergence-comparison.
$root_url = "http://my.gpx.tools/mygpxpod";

// gpx-divergence-comparison instance root URL to call in case of comparison
$gpxcomp_root_url = "http://my.gpx.tools/mygpxcomp";
?>
