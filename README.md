GpxPod : GPX Pretty Organisation and Display
=================

# Introduction
---

GPXPOD is a web tool to display gpx files collections, view elevation profiles and tracks stats, filter tracks...

GPXPOD is **still under development.**

Future features :
- Draw tracks colored by various information like speed, slope, elevation...

Here is a [demo](http://pluton.cassio.pe/~demo/gpxpod)

GPXPOD uses [Leaflet](http://leafletjs.com),
[Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster),
[Leaflet.Elevation](https://github.com/MrMufflon/Leaflet.Elevation),
[Leaflet.sidebar-v2](https://github.com/turbo87/sidebar-v2/),
[Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder),
[Leaflet.MiniMap](https://github.com/Norkart/Leaflet-MiniMap),
[Leaflet Mouse Position](https://github.com/ardhi/Leaflet.MousePosition),
[JQuery](http://jquery.com/), [python-geojson](https://github.com/frewsxcv/python-geojson),
[gpxpy](https://github.com/tkrajina/gpxpy)...

## Table of content
---
1. [Screenshots](#screenshots)
2. [Requirements](#requirements)
3. [Installation](#installation)
4. [Usage](#usage)

# Screenshots
---
Beautifull, fast and easy navigation with markercluster plugin :
![Gpxpod screenshot2](https://gitlab.com/eneiluj/gpxpod/raw/master/screenshots/gpxdisplay2.png)


Nice interative elevation profile with Leaflet.Elevation :
![Gpxpod screenshot1](https://gitlab.com/eneiluj/gpxpod/raw/master/screenshots/gpxdisplay1.png)


Nice retractable [sidebar](https://github.com/turbo87/sidebar-v2/) which becomes transparent when hover on table rows to *quickly* display tracks overviews
![Gpxpod screenshot3](https://gitlab.com/eneiluj/gpxpod/raw/master/screenshots/gpxdisplay3.png)


Ability to draw multiple tracks. Each track is drawn with a different color. Click on a track triggers a popup displaying track's statistics.
![Gpxpod screenshot4](https://gitlab.com/eneiluj/gpxpod/raw/master/screenshots/gpxdisplay4.png)

# Requirements
---
You'll need [gpxpy](https://github.com/tkrajina/gpxpy) and [python-geojson](https://github.com/frewsxcv/python-geojson) to be installed on your web server.

You'll also need a web server and PHP 5.

# Installation
---
Copy "www" folder somewhere in your webserver path.

Copy gpxpod.py somewhere outside your webserver path.

Adjust variables in conf.php :

```php
// Path to gpxpod.py in order to produce markers and convert tracks to geojson.
$path_to_gpxpod = "/path/to/gpxpod.py";

// Path that will be scanned to find folders containing gpx files.
// !! All folders in this path must be writable by the user running the 
// web server to write markers and geojson files !!
$data_folder = "/path/to/data/folder";

// Root URL to gpxpod, used to pass GPX urls to an instance of gpx-divergence-comparison.
$root_url = "http://my.gpx.tools/mygpxpod";

// gpx-divergence-comparison instance root URL to call in case of comparison
$gpxcomp_root_url = "http://my.gpx.tools/mygpxcomp";
```

# Usage
---
Choose a folder to display, then submit.

In the side panel, a table lists all visible track markers by showing the name, date and total distance.
Hover on a row to quickly display the track without changing map position or zoom.
Check a row to display a track and center map on it.

Click on the "Compare selected tracks" to pass checked tracks to a gpx-divergence-comparison instance.

Click on tracks or markers for detailed information and for drawing elevation profile.
