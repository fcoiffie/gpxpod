#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, math, os
import json
import gpxpy, gpxpy.gpx, geojson

def format_time_seconds(time_s):
    if not time_s:
        return 'n/a'
    minutes = math.floor(time_s / 60.)
    hours = math.floor(minutes / 60.)

    return '%s:%s:%s' % (str(int(hours)).zfill(2), str(int(minutes % 60)).zfill(2), str(int(time_s % 60)).zfill(2))

def distance(p1, p2):
    """ return distance between these two gpx points in meters
    """

    lat1 = p1.latitude
    long1 = p1.longitude
    lat2 = p2.latitude
    long2 = p2.longitude

    if (lat1 == lat2 and long1 == long2):
        return 0

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc*6371000

def gpxTracksToGeojson(gpx_content, name):
    """ converts the gpx string input to a geojson string
    """
    gpx = gpxpy.parse(gpx_content)

    featureList = []
    for waypoint in gpx.waypoints:
        featureList.append(
            geojson.Feature(
                id=waypoint.name,
                properties=None,
                geometry=geojson.Point((waypoint.longitude, waypoint.latitude))
            )
        )

    coordinates = []
    for track in gpx.tracks:
        lastPoint = None
        for segment in track.segments:
            for point in segment.points:
                if not point.elevation:
                    point.elevation = 0
                coordinates.append((point.longitude, point.latitude, int(point.elevation))) 

        featureList.append(
            geojson.Feature(
                id=name,
                properties=None,
                geometry=geojson.LineString(coordinates)
            )
        )
        fc = geojson.FeatureCollection(featureList, id=name)
        return geojson.dumps(fc)

def getMarkerFromGpx(gpx_content, name):
    """ return marker string that will be used in the web interface
        each marker is : [x,y,filename,distance,duration,datebegin,dateend,poselevation,negelevation]
    """
    lat = 'null'
    lon = 'null'
    total_distance = 0
    total_duration = 'null'
    date_begin = 'null'
    date_end = 'null'
    pos_elevation = 0
    neg_elevation = 0
    min_elevation = 'null'
    max_elevation = 'null'
    max_speed = 0
    avg_speed = "null"
    north = None
    south = None
    east = None
    west = None


    isGoingUp = False
    lastDeniv = None
    upBegin = None
    downBegin = None

    gpx = gpxpy.parse(gpx_content)
    for track in gpx.tracks:
        featureList = []
        lastPoint = None
        pointIndex = 0
        for segment in track.segments:
            for point in segment.points:
                lastTime = point.time
                if pointIndex == 0:
                    lat = point.latitude
                    lon = point.longitude
                    date_begin = point.time
                    downBegin = point.elevation
                    min_elevation = point.elevation
                    max_elevation = point.elevation
                    north = point.latitude
                    south = point.latitude
                    east = point.longitude
                    west = point.longitude
                if point.latitude > north:
                    north = point.latitude
                if point.latitude < south:
                    south = point.latitude
                if point.longitude > east:
                    east = point.longitude
                if point.longitude < west:
                    west = point.longitude
                if point.elevation < min_elevation:
                    min_elevation = point.elevation
                if point.elevation > max_elevation:
                    max_elevation = point.elevation
                if lastPoint != None and point.time and lastPoint.time:
                    t = (point.time - lastPoint.time).seconds
                    if t != 0:
                        speed = distance(lastPoint, point) / t
                        speed = speed / 1000
                        speed = speed * 3600
                        if speed > max_speed:
                            max_speed = speed
                if lastPoint != None and point.elevation and lastPoint.elevation:
                    deniv = point.elevation - lastPoint.elevation
                    total_distance += distance(lastPoint, point)
                if lastDeniv != None and point.elevation and lastPoint.elevation:
                    # we start to go up
                    if (isGoingUp == False) and deniv > 0:
                        upBegin = lastPoint.elevation
                        isGoingUp = True
                        neg_elevation += (downBegin - lastPoint.elevation)
                    if (isGoingUp == True) and deniv < 0:
                        # we add the up portion
                        pos_elevation += (lastPoint.elevation - upBegin)
                        isGoingUp = False
                        downBegin = lastPoint.elevation
                # update vars
                if lastPoint != None and point.elevation and lastPoint.elevation:
                    lastDeniv = deniv

                lastPoint = point
                pointIndex += 1

        if not max_elevation:
            max_elevation = "null"
        else:
            max_elevation = '%.2f'%max_elevation
        if not min_elevation:
            min_elevation = "null"
        else:
            min_elevation = '%.2f'%min_elevation
        date_end = lastTime
        if date_end and date_begin:
            totsec = (date_end - date_begin).total_seconds()
            #total_duration =str(date_end - date_begin)
            total_duration = '%.2i:%.2i:%.2i'%(totsec // 3600, totsec % 3600 // 60, totsec % 60)
            avg_speed = (total_distance) / totsec
            avg_speed = avg_speed / 1000
            avg_speed = avg_speed * 3600
            avg_speed = '%.2f'%avg_speed
        else:
            total_duration = "???"

        # auto analye from gpxpy
        # we consider every segment under 0.9 km/h as a stop time
        moving_time, stopped_time, moving_distance, stopped_distance, moving_max_speed = gpx.get_moving_data(0.9)

        # determination of real moving average speed from moving time
        moving_avg_speed = 0
        if moving_time != 0:
            moving_avg_speed = (total_distance) / moving_time
            moving_avg_speed = moving_avg_speed / 1000
            moving_avg_speed = moving_avg_speed * 3600
            moving_avg_speed = '%.2f'%moving_avg_speed

        return '[%s, %s, "%s", %s, "%s", "%s", "%s", %s, %s, %s, %s, %s, %s, "%s", "%s", %s, %s, %s, %s, %s]'%(
                lat,
                lon,
                name,
                '%.2f'%total_distance,
                total_duration,
                date_begin, 
                date_end,
                '%.2f'%pos_elevation,
                '%.2f'%neg_elevation,
                min_elevation,
                max_elevation,
                '%.2f'%max_speed,
                avg_speed,
                format_time_seconds(moving_time),
                format_time_seconds(stopped_time),
                moving_avg_speed,
                north,
                south,
                east,
                west
                )


if __name__ == "__main__":
    path = sys.argv[1]
    if not os.path.exists(path):
        sys.stderr.write('%s does not exist'%path)
        sys.exit(1)

    files = [ os.path.join(path,f) for f in os.listdir(path) if (os.path.isfile(os.path.join(path,f)) and f.endswith('.gpx')) ]
    markers = []

    for i,f in enumerate(files):
        fd = open(f,'r')
        content = fd.read()
        fd.close()

        # write GEOJSON
        if not os.path.exists('%s.geojson'%f):
            print('Processing %s [%s/%s] ...'%(os.path.basename(f),(i+1),len(files))),
            gf = open('%s.geojson'%f, 'w')
            gf.write(gpxTracksToGeojson(content, os.path.basename(f)))
            gf.close()
            print('Done')

        # build marker
        markers.append(getMarkerFromGpx(content,os.path.basename(f)))

    print('Writing markers')
    # write marker file
    f = open(os.path.join(path,'markers.txt'), 'w')
    f.write('{"markers" : [\n')
    f.write(',\n'.join(markers))
    f.write('\n]}')
    print('Done')
