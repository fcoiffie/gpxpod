#!/usr/bin/python

import srtm,sys
import gpxpy

def usage():
    usage = "Give me a file.gpx and i will use SRTM to correct elevation data and produce file.srtm.gpx \n"
    sys.stderr.write(usage)
    sys.exit(1)

if len(sys.argv) < 2:
    usage()

gpx = gpxpy.parse(open(sys.argv[1]))
elevation_data = srtm.get_data()
elevation_data.add_elevations(gpx, smooth=True)
ff = open(sys.argv[1].replace(".gpx",".srtm.gpx"),"w")
ff.write(gpx.to_xml())
ff.close()
